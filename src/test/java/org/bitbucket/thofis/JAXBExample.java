package org.bitbucket.thofis;

import org.bitbucket.thofis.model.AddressType;
import org.bitbucket.thofis.model.GenderType;
import org.bitbucket.thofis.model.Person;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.GregorianCalendar;

public class JAXBExample {
    public static void main(String[] args) {

        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(Person.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(person(), System.out);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static Person person() throws Exception {
        Person personType = new Person();




        personType.setFirstName("Thomas");
        personType.setLastName("Fischer");
        GregorianCalendar gcal = new GregorianCalendar();
        XMLGregorianCalendar xgcal = DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(gcal);
        personType.setDateOfBirth(xgcal);
        personType.setGender(GenderType.MALE);

        AddressType addressType = new AddressType();
        addressType.setCity("Regensburg");
        addressType.setStreet("Hermann-Köhl-Str. 20a");
        addressType.setZip("93049");

        personType.getAddress().add(addressType);

        return personType;

//        final JAXBElement<PersonType> person = new ObjectFactory().createPerson(personType);
//
//        return person;
    }
}