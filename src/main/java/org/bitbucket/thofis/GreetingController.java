package org.bitbucket.thofis;

import org.bitbucket.thofis.model.AddressType;
import org.bitbucket.thofis.model.GenderType;
import org.bitbucket.thofis.model.Person;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.GregorianCalendar;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }

    @RequestMapping(value = "/person", produces = {
            "application/xml", "application/json"
    })
    public Person personType() throws Exception {
        Person person = getPerson();
        return person;
    }


    private Person getPerson() throws DatatypeConfigurationException {
        Person person = new Person();
        person.setFirstName("John");
        person.setLastName("Doe");
        GregorianCalendar gcal = new GregorianCalendar();
        XMLGregorianCalendar xgcal = DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(gcal);
        person.setDateOfBirth(xgcal);
        person.setGender(GenderType.MALE);

        AddressType addressType = new AddressType();
        addressType.setCity("Gotham City");
        addressType.setStreet("Hampton Blvd. 20");
        addressType.setZip("93049");

        person.getAddress().add(addressType);

        addressType = new AddressType();
        addressType.setCity("Somewhere");
        addressType.setStreet("Huston Str. 20");
        addressType.setZip("12345");

        person.getAddress().add(addressType);

        return person;
    }

//    @RequestMapping(value = "/personstring", produces = "application/xml")
//    public String personString() throws Exception {
//        PersonType personType = new PersonType();
//        personType.setFirstName("Thomas");
//        personType.setLastName("Fischer");
//        GregorianCalendar gcal = new GregorianCalendar();
//        XMLGregorianCalendar xgcal = DatatypeFactory.newInstance()
//                .newXMLGregorianCalendar(gcal);
//        personType.setDateOfBirth(xgcal);
//        personType.setGender(GenderType.MALE);
//
//        AddressType addressType = new AddressType();
//        addressType.setCity("Regensburg");
//        addressType.setStreet("Hermann-Köhl-Str. 20a");
//        addressType.setZip("93049");
//
//        personType.getAddresses().add(addressType);
//
//        final JAXBElement<PersonType> person = new ObjectFactory().createPerson(personType);
//
//        JAXBContext jaxbContext = JAXBContext.newInstance(PersonType.class);
//        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//
//        // output pretty printed
//        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//
//        java.io.StringWriter personString = new StringWriter();
//        jaxbMarshaller.marshal(person, personString);
//
//        return personString.toString();
//    }
//
//    @RequestMapping(value = "/persontype", produces = "application/xml")
//    public PersonType personType() throws Exception {
//        PersonType personType = new PersonType();
//        personType.setFirstName("Thomas");
//        personType.setLastName("Fischer");
//        GregorianCalendar gcal = new GregorianCalendar();
//        XMLGregorianCalendar xgcal = DatatypeFactory.newInstance()
//                .newXMLGregorianCalendar(gcal);
//        personType.setDateOfBirth(xgcal);
//        personType.setGender(GenderType.MALE);
//
//        AddressType addressType = new AddressType();
//        addressType.setCity("Regensburg");
//        addressType.setStreet("Hermann-Köhl-Str. 20a");
//        addressType.setZip("93049");
//
//        personType.getAddresses().add(addressType);
//
//        return personType;
//    }
//
//    @RequestMapping(value = "/personjson", produces = "application/json")
//    public PersonType personJson() throws Exception {
//        PersonType personType = new PersonType();
//        personType.setFirstName("Thomas");
//        personType.setLastName("Fischer");
//        GregorianCalendar gcal = new GregorianCalendar();
//        XMLGregorianCalendar xgcal = DatatypeFactory.newInstance()
//                .newXMLGregorianCalendar(gcal);
//        personType.setDateOfBirth(xgcal);
//        personType.setGender(GenderType.MALE);
//
//        AddressType addressType = new AddressType();
//        addressType.setCity("Regensburg");
//        addressType.setStreet("Hermann-Köhl-Str. 20a");
//        addressType.setZip("93049");
//
//        personType.getAddresses().add(addressType);
//
//        return personType;
//    }
//
//    @RequestMapping(value = "/personjaxb", produces = "application/xml")
//    public JAXBElement<PersonType> personJaxBElement() throws Exception {
//        PersonType personType = new PersonType();
//        personType.setFirstName("Thomas");
//        personType.setLastName("Fischer");
//        GregorianCalendar gcal = new GregorianCalendar();
//        XMLGregorianCalendar xgcal = DatatypeFactory.newInstance()
//                .newXMLGregorianCalendar(gcal);
//        personType.setDateOfBirth(xgcal);
//        personType.setGender(GenderType.MALE);
//
//        AddressType addressType = new AddressType();
//        addressType.setCity("Regensburg");
//        addressType.setStreet("Hermann-Köhl-Str. 20a");
//        addressType.setZip("93049");
//
//        personType.getAddresses().add(addressType);
//
//        return new ObjectFactory().createPerson(personType);
//
//    }

}